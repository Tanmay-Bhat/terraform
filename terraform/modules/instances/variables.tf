
variable "project_id" {
  description = "The project ID to host the network in"
  default     = "qwiklabs-gcp-01-42f3a17a51c7"
}

variable "region" {
    description = "the Region in which your resources exist"
    default ="us-central1"
}

variable "zone" {
    description = "the zone in which your resources exist"
    default = "us-central1-a"
}
